import Foundation
import AlamofireObjectMapper
import ObjectMapper

public final class NewsObject: Mappable {

  private struct SerializationKeys {
    static let dateText = "date"
    static let titleText = "title"
    static let picLink = "pic"
    static let bodyText = "text"
  }

  public var dateText: String?
  public var titleText: String?
  public var picLink: String?
  public var bodyText: String?

  public required init?(map: Map) {}

  public func mapping(map: Map) {
    dateText <- map[SerializationKeys.dateText]
    titleText <- map[SerializationKeys.titleText]
    picLink <- map[SerializationKeys.picLink]
    bodyText <- map[SerializationKeys.bodyText]
  }
  
}
