import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class MainRequest {
  
  class func fire(completion: @escaping (_ parameter: [NewsObject]) -> Void) {
    if Reachability.isConnectedToNetwork() {
      Alamofire.request("https://tucannewspukan.firebaseio.com/news.json", method: .get)
        .validate(statusCode: 200..<300)
        .responseArray { (response: DataResponse<[NewsObject]>) in
          if (response.result.error == nil) {
            completion(response.result.value!)
          } else {
//            debugPrint("HTTP Request failed: \(response.result.error)")
          }
      }
    } else {
      print("NO INTERNET FOR U")
    }
  }
  
}
