import UIKit
import Kingfisher
import NVActivityIndicatorView

class ListCell: UITableViewCell {
  
  @IBOutlet private weak var indicator: NVActivityIndicatorView!
  @IBOutlet private weak var containerView: UIView!
  @IBOutlet private weak var newsTitleLabel: UILabel!
  @IBOutlet private weak var newsImageView: UIImageView!
  @IBOutlet private weak var newsDateLabel: UILabel!
  @IBOutlet private weak var newsBodyLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    containerView.layer.shadowColor = UIColor.black.cgColor
    containerView.layer.shadowOpacity = 0.3
    containerView.layer.shadowOffset = CGSize.zero
    containerView.layer.shadowRadius = 3
    
    indicator.startAnimating()
  }
  
  func setData(object: NewsObject) {
    newsTitleLabel.text = object.titleText
    newsDateLabel.text = object.dateText
    newsBodyLabel.text = object.bodyText
    
    let imgUrl = URL(string: object.picLink!)
    newsImageView.kf.setImage(with: imgUrl)
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    self.selectionStyle = .none
  }
  
}
