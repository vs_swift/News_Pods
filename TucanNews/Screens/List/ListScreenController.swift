import UIKit

class ListScreenController: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
  @IBOutlet private weak var tableView: UITableView!
  
  private var tableArray: [NewsObject] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.title = "News"
  }
  
  override func viewDidAppear(_ animated: Bool) {
    
    MainRequest.fire { (receivedData) in
      self.tableArray = receivedData
      self.tableView.reloadData()
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return tableArray.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell") as! ListCell
    
    cell.setData(object: tableArray[indexPath.row])
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.performSegue(withIdentifier: "ShowDetails", sender: tableArray[indexPath.row])
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "ShowDetails" {
      let destinationVC = segue.destination as! DetailsController
      destinationVC.dataObject = sender as! NewsObject
    }
  }
  
}
