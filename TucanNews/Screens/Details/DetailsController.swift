import UIKit
import Kingfisher

class DetailsController: UIViewController {
  
  var dataObject: NewsObject?
  
  @IBOutlet weak var underlayImageView: UIImageView!
  @IBOutlet private weak var newsImageView: UIImageView!
  @IBOutlet private weak var newsTitleLabel: UILabel!
  @IBOutlet private weak var newsDateLabel: UILabel!
  @IBOutlet private weak var newsBodyLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let url = URL(string: (dataObject?.picLink)!)
    newsImageView.kf.setImage(with: url!)
    underlayImageView.kf.setImage(with: url!)
    
    newsTitleLabel.text = dataObject?.titleText
    newsDateLabel.text = dataObject?.dateText
    newsBodyLabel.text = dataObject?.bodyText
  }
}
